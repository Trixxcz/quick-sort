<?php

namespace App\Model;

class Sorter
{
/** Sort an array with ascending order quick-sort using recursive call and merging left side, pivot and right side of array
 * 
 * @param type $array Input array from Homepage presenter
 * @return type Array sorted in ascending order
 */    
    public function quicksortAsc($array)
    {
        if (count($array) <= 1)
        {
            return $array;
        }
        else
        {
            $leftside = [];
            $rightside = [];
            $pivot = $array[0];
            for($position = 1; $position < count($array); $position++)
            {
                if ($array[$position] < $pivot)
                {
                    $leftside[] = $array[$position];
                }
                else
                {
                    $rightside[] = $array[$position];
                }
            }
            return array_merge($this->quicksortAsc($leftside), array($pivot), $this->quicksortAsc($rightside));
        }
    }

/** Sort an array with descending order quick-sort using recursive call and merging left side, pivot and right side of array
 * 
 * @param type $array Input array from Homepage presenter
 * @return type Array sorted in descending order
 */    
    public function quicksortDesc($array)
    {
        if (count($array) <= 1)
        {
            return $array;
        }
        else
        {
            $leftside = [];
            $rightside = [];
            $pivot = $array[0];
            for($position = 1; $position < count($array); $position++)
            {
                if ($array[$position] > $pivot)
                {
                    $leftside[] = $array[$position];
                }
                else
                {
                    $rightside[] = $array[$position];
                }
            }
            return array_merge($this->quicksortDesc($leftside), array($pivot), $this->quicksortDesc($rightside));
        }
    }
}

