SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

DROP DATABASE IF EXISTS `quick-sort`;

CREATE DATABASE `quick-sort`;

USE `quick-sort`;

DROP TABLE IF EXISTS `sorts`;

CREATE TABLE `sorts` (
`sortId` int(11) NOT NULL AUTO_INCREMENT,
`sorted` varchar(255),
`unsorted` varchar(255),
`ip` varchar(70),
`browser` varchar(255),
PRIMARY KEY (`sortId`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

