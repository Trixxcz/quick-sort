<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use App\Model\Sorter;

class HomepagePresenter extends BasePresenter
{
    private $database;
    private $sorter;
    private $container;
    private $browser;
    
    /** Constructor
     * 
     * @param Nette\Database\Context $database Database context
     * @param Sorter $sorter Sorter model
     * @param Nette\Http\Request $container HttpRequest container
     */
    public function __construct(Nette\Database\Context $database, Sorter $sorter, Nette\Http\Request $container)
    {
        $this->database = $database;
        $this->sorter = $sorter;
        $this->container = $container;
        $this->browser = $this->container->getHeader('User-Agent');
    }
    
    /** Creating of InputForm component
     * 
     * @return Form User input
     */

    protected function createComponentInputForm()
    {
        $type = [
            'asc' => 'vzestupně',
            'desc' => 'sestupně'
        ];
        $form = new Form;
        $form->addText('input', 'Zadejte čísla k setřízení:')
                ->setRequired('Zadejte čísla k setřízení!');
        $form->addRadioList('sorttype', 'Setřídit:', $type)
                ->getSeparatorPrototype()->setName(NULL);        
        $form->addSubmit('send', 'Proveď');
        $form->onSuccess[] = [$this, 'inputFormSucceeded'];
        
        return $form;    
    }

    /** Validating and database filling function
     * 
     * @param type $form Form from InputForm component
     * @param type $values Values from form
     */
    
    public function inputFormSucceeded($form, $values)
    {
        $sortId = $this->getParameter('sortId');
        $unsorted = $values->input;
        
        if (preg_match("/^[0-9,.-]+$/", $unsorted))
        {
            switch ($values->sorttype)
            {
                case "asc":
                    $sorted = $this->sorter->quicksortAsc(explode(',', $unsorted));
                    break;
                case "desc":
                    $sorted = $this->sorter->quicksortDesc(explode(',', $unsorted));
                    break;
                default:
                    $this->flashMessage('Vyberte prosím zda se má třídit vzestupně nebo sestupně.');
                    $this->redirect('Homepage:');
                    break;                                
            }
        
            $this->database->table('sorts')->insert([
                'sortId' => $sortId,
                'sorted' => implode(',', $sorted),
                'unsorted' => $unsorted,
                'ip' => $this->container->getRemoteAddress(),
                'browser' => $this->browser
                ]);
        
            $this->flashMessage('Zpracování proběhlo úspěšně.');
            $this->redirect('Homepage:');
        
        }
        else
        {
            $this->flashMessage('Zpracování bylo neúspěšné, zadaný řetězec obsahuje nepovolené znaky.');
            $this->redirect('Homepage:');
        }
    }
}
